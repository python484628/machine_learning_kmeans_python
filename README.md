**Step 1**

Creating a simple toy dataset made of blobs.

**Step 2**

K-means clustering.

**Step 3**

The effect of noise in data.

**Step 4**

The effect of the initialization.

**Step 5**

How to fix the K ?

**Step 6**

The effect of the shape of data.

**Step 7**

Hierarchical clustering.


Author : Marion Estoup

E-mail : marion_110@hotmail.fr

February 2021

